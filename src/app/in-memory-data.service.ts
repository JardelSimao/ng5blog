import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const posts = [
      { id: 11, name: 'Post 1' },
      { id: 12, name: 'Post 2' },
      { id: 13, name: 'Post 3' },
      { id: 14, name: 'Post 4' },
      { id: 15, name: 'Post 5' },
      { id: 16, name: 'Post 6' },
      { id: 17, name: 'Post 7' },
      { id: 18, name: 'Post 8' },
      { id: 19, name: 'Post 9' },
      { id: 20, name: 'Post 10' }
    ];
    return {posts};
  }
}
